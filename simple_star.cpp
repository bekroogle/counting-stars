#include <iostream>
#include <vector>
#include <string>

using namespace std;

int height, width;

void expand_star(string[], int, int);

int main()
{
	int star_count = 0;
	string* lines;
	string buf;
	int case_no = 0;

	while(!cin.eof()) {
		star_count = 0;

		cin >> height >> width;
		if (cin.fail()) {
			break;
		}
		cin.ignore();

		// Allocate dynamic memory for image
		lines = new string[height];

		// Read image into memory
		for (int i = 0; i < height; i++) {
			getline(cin, buf);
			lines[i] = buf;
		}

			// For each unvisited pixel:
			for (int h = 0; h < height; h++) {
				for (int w = 0; w < width; w++) {
					// for each unvisited white pixel
					if (lines[h][w] == '-') {
						star_count++;

						// Mark pixel as visited.
						lines[h][w] = '+';

						expand_star(lines, h, w);
					}

				}
			}
			cout << "Case " << ++case_no << ": " << star_count << ".\n";
		}

}
		


	




void expand_star(string lines[], int y, int x) {
	// for each unvisited neighbor:
	
	// Check left
	if (x > 0) {
		if (lines[y][x-1] == '-') {
			lines[y][x-1] = '+';
			expand_star(lines, y, x - 1);
		}
	}

	// Check bottom
	if (y < height - 1) {
		if (lines[y+1][x] == '-') {
			lines[y+1][x] = '+';
			expand_star(lines, y+1, x);
		}
	}

	// Check right
	if (x < width - 1) {
		if (lines[y][x+1] == '-') {
			lines[y][x+1] = '+';
			expand_star(lines, y, x+1);
		}
	}

	// Check top
	if (y > 0) {
		if (lines[y-1][x] == '-') {
			lines[y-1][x] = '+';
			expand_star(lines, y-1, x);
		}
	}

}