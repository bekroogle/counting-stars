#include <iostream>
#include <string>
#include <vector>

using namespace std;

const int MINPTS = 1;
const int EPS = 1;
const int NOISE = -1;

// Globally accessible dataset (I know it's bad practice.)


class Node {
public:
	Node(int fx, int fy) { x = fx; y = fy; visited = false;} 
	
	void visit() { visited = true; }
	void add_to(int c_id) {cluster_id = c_id;}
	// Const member functionsn

	int get_x() const {return x;}
	int get_y() const { return y;}
	int get_cluster() const { return cluster_id; }
	// Dist uses Manhattan distance.
	int dist(const Node& other) { return (other.y - this->y) + (other.x - this->x ); }
	bool is_visited() const { return visited; }

private:
	int x;
	int y;
	int cluster_id;
	bool visited;
};

vector<Node> nodes;

int cluster(vector<Node> dataset, int min_pts, int eps);
vector<Node> region_query(Node, int);
void expand_cluster(Node, vector<Node>, int, int, int);

int main()
{
	// Declarations
	int height, width;
	string * lines;
	string buf;
	Node * node_ptr;
	
	vector<Node>::iterator nit;

	// Get image size
	cin >> height >> width;
	cin.ignore();

	// Allocate dynamic memory for image
	lines = new string[height];

	// Read image into memory
	for (int i = 0; i < height; i++) {
		getline(cin, buf);
		lines[i] = buf;
	}

	// Detect starlight pixels from image matrix
	for (int i = 0; i < height; i++) {
		for (int j = 0; j < width; j++) {
			if (lines[i][j] == '-') {
				node_ptr = new Node(j, i); 
				nodes.push_back(*node_ptr);
				delete node_ptr;
			}			
		}
	}

	// Cluster star pixels
	
	cout << endl << cluster(nodes, MINPTS, EPS);
	for (nit = nodes.begin(); nit != nodes.end(); nit++) {
		cout << nit->get_x() << ", " << nit->get_y() << "Cluster: " << nit->get_cluster() << endl;
	}
	return 0;
}

int cluster(vector<Node> nodes, int min_pts, int eps)
{
	vector<Node> neighbors;
	vector<Node>::iterator nit;
	int c_id = 0;	// Cluster ID

	// For each unvisited Node in nodes:
	for (nit = nodes.begin(); nit != nodes.end(); ++nit) {
		if (!nit->is_visited()) {
			
			// Set node as visited
			nit->visit();
			neighbors = region_query(*nit, eps);
			if (neighbors.size() < min_pts) {
				nit->add_to(NOISE);
			} else {
				c_id++;
				expand_cluster(*nit, neighbors, c_id, eps, min_pts);
			}
		}

	}
	return c_id;
}

void expand_cluster(Node n, vector<Node> neighbors, int c_id, int eps, int min_pts) {
	vector<Node> neighbors_prime;
	
	// Assign node to cluster
	n.add_to(c_id);

	// For each node in neighbors
	for (vector<Node>::iterator nit_prime = neighbors.begin(); nit_prime != neighbors.end(); ++nit_prime) {
		
		// If it's unvisited
		if (!nit_prime->is_visited()) {
			
			// Mark it as visited
			nit_prime->visit();

			// Find its neighbors
			neighbors_prime = region_query(*nit_prime, eps);

			// If the neighborhood is dense
			if (neighbors_prime.size() >= min_pts) {

				// Add this point's neighborhood to the neighborhood of node n.
				for (vector<Node>::iterator nit = neighbors.begin(); nit != neighbors.end(); nit++) {
					neighbors_prime.push_back(*nit);
				}
			}

			// If the node is not a member of a cluster
			if (nit_prime->get_cluster() == -1) {
				nit_prime->add_to(c_id);
			}
		}

	}

}

// Returns all points within Node n's epsilon-neighborhood.
vector<Node> region_query(Node n, int eps) {
	vector<Node> temp;
	for (vector<Node>::iterator new_itr = nodes.begin(); new_itr != nodes.end(); ++new_itr) {
		if (n.dist(*new_itr) <= 1) {
			temp.push_back(*new_itr);
		}
	}
	return temp;
}

